# aplicacion express polymer:

se trata de hacer una aplicacion express que sirva como contenido una aplicacion polymer

*previo (instalacion)*
~~~
npm install express-generator -g
~~~
# 1. empezamos creando una aplicacion express tipica 
-
~~~   
express polymerExpress
~~~

cambiarnos al directorio y bajarse las dependencias
-
~~~ 
     cd polymerExpress && npm install
~~~

# 2. crear tu repo
-
 
*ir a bitbucket 

*repository / create a repository 

*darle nombre 

*desmarcar privado si aplica 

*pulsar boton crear 

*cambiar al directorio donde tienes el codigo 
-
~~~ 
cd /path/to/your/project
~~~
*indicar que quieres un proyecto git 
-
~~~ 
git init
~~~
*en la pagina de despues de crear el repo copiar la ruta para aÃƒÂ±adir al repo (algo como esto )*
-
~~~ 
 git remote add origin    git remote add origin https://guzmanpaniagua@bitbucket.org/guzmanpaniagua/polymerexpress.git
~~~
crear el fichero para ignoraar archivos y rutas en git 
-
~~~ 
touch .gitignore
~~~

poner como contenido para que excluya las dependencias de node 
-
~~~ 
 node_modules/
~~~
comprobar que va a subir las cosas 
-
~~~ 
       git status
~~~
añadir los ficheros al git 
-
~~~ 
       git add .
~~~
hace run comit para decirle a git que quieres los cambios
-
~~~ 
    git commit -m "Initial commit"
~~~

 
subir los cambios al repositorio 
-
~~~ 
    git push -u origin master
~~~

# 3) crear una aplicacion polymer
-
Ahora crearemos una aplicacion polymer, para tener el codigo separado pero en el mismo repositorio, el fuente de la aplicacio polymer estara en una carpeta llamada client.

los pasos para usar el cli estan explicados en 
-
~~~ 
https://www.polymer-project.org/1.0/docs/tools/polymer-cli
~~~ 

Instalamos las dependencias y el polymer cli

Install the latest version of Bower.
-
~~~ 
npm install -g bower
~~~ 

Install Polymer CLI.
-
~~~ 
npm install -g polymer-cli
~~~ 

creamos el directorio para el codigo de la aplicacion
-
~~~ 
mkdir client
~~~ 

Nos pasamos al directorio 
-
~~~ 
cd client
~~~ 

En el directorio client usamos el polymer cli para inicializar una aplicación 
-
~~~ 
polymer init
~~~ 

elegimos "starter-kit" para tener navegación y algo ya hecho
-
~~~ 
? Which starter template would you like to use?
  1) element: A blank element template
  2) application: A blank application template
  3) shop: The "Shop" Progressive Web App demo
  4) starter-kit: A starter application template, with navigation and "PRPL pat
tern" loading
~~~ 

construimos la aplicacio para tenerla disponible
-
~~~ 
polymer build
~~~

y vemos que funciona 
-
~~~ 
polymer serve
~~~

hay que añadir al .gitignore 
-
~~~ 
client/build/
client/bower_components/
~~~ 

incluimos lo snuevos archivos que pudiera haber
-
~~~ 
       git add .
~~~
hace run comit para decirle a git que quieres los cambios
-
~~~ 
    git commit -m "added client base side"
~~~

subir los cambios al repositorio 
-
~~~ 
    git push -u origin master
~~~

# 4) entendiendo el flujo de trabajo 
-
Para poder hacer cambios tenemos que 
-
~~~ 
polymer build
~~~

eso generara una carpeta del tipo 
-
~~~ 
C:\proyectos\polymerExpress\client\build\bundled
~~~ 

volvemos al directorio principal
-
~~~ 
cd ..
~~~ 

y ejecutamos el express
-
~~~ 
npm start 
~~~ 

si arrancamos en el navegador localhost:3000 deberiamos de ver la pagina de welcome to express

ahora hay que indicarle a express que queremos la carpeta generada de construccion, incluimos en el app.js
-
~~~ 
app.use(express.static(path.join(__dirname, 'client/build/bundled')));
~~~ 

y cambiamos en routes el archivo index para ue devuelva nuestro index 
-
~~~ 
var express = require('express');
var router = express.Router();

/* GET home page. */

router.get('/', (req, res) => {
   res.sendFile('index.html', {
     root: 'client/build/bundled'
   });
});
module.exports = router;
~~~ 


incluimos lo snuevos archivos que pudiera haber
-
~~~ 
       git add .
~~~
hace run comit para decirle a git que quieres los cambios
-
~~~ 
    git commit -m "added express config to use polymer app"
~~~
 
subir los cambios al repositorio 
-
~~~ 
    git push -u origin master
~~~

# 5) añadir parte rest servidora 
-
dentro del directorio routes crear un nuevo ficherotodos.js y pon de contenido 
-
~~~
// una lista de items para devolver 
var listItems =
[
    {
        "id": 1,
        "text": "Get Up"
    },
    {
        "id": 2,
        "text": "Brush teeth"
    },
    {
        "id": 3,
        "text": "Get Milk"
    },
    {
        "id": 4,
        "text": "Prepare coffee"
    },
    {
        "id": 5,
        "text": "Have a hot cup of coffee"
    }
];
// un metodo para devolver toda la lista 
exports.list = function(req, res){   
    res.json(listItems);
};

// un metodo para obtener un objketo de la lista por id
exports.get = function(req, res){ 
   var item = {};
   if (req.params.id){
     item = listItems.find(function(item) {
          return item.id == req.params.id;
      });
   }
   if(!item){
      item = {"item":"not found"} 
   }
   res.json(item);
};

// un metodo para incluri un elemento a la lista 
exports.addToList = function (req, res) {
    var maxId = listItems.length;
    var newTodoItem = { "id": maxId.id + 1, "text": req.body.text };
    listItems.push(newTodoItem);
    res.json({ success: true, item: newTodoItem });
};
~~~

modifica el app.js para incluir las rutas para las funciones creadas
-
~~~
// import new functionality todos
var todos = require('./routes/todos');

// route handler for get all todos
app.get('/api/todos/:id', todos.get);

// route handler for get all todos
app.get('/api/todos', todos.list);

// route handler for set a new todo
app.post('/api/todos', todos.addToList);
~~~

aranca el servidor y en otra pestaña prueba a ver los json 
-
~~~
http://localhost:3000/api/todos
~~~


incluimos lo snuevos archivos que pudiera haber
-
~~~ 
       git add .
~~~
hace run comit para decirle a git que quieres los cambios
-
~~~ 
    git commit -m "added todo rest operations"
~~~
 
subir los cambios al repositorio 
-
~~~ 
    git push -u origin master
~~~




6) modificar el componenet de polymer para que llame al api 

ir al directorio client 
-
~~~ 
cd client
~~~ 

poner el componenet de iron ajax como dependencia
-
~~~ 
bower install --save PolymerElements/iron-ajax
~~~ 

incluir el impor en la pagina donde lo vamos a usar my-view1.html
-
~~~ 
<link rel="import" href="../bower_components/iron-ajax/iron-ajax.html">
~~~ 

envolver la template existente en una capa y poner el componenet de iron ajax 
-
~~~ 
<div>
 <iron-ajax
            id="requestTodos"
            url="/api/todos"
            handle-as="json"
            on-response="handleResponse">
        </iron-ajax>
   
....

<div>
~~~ 


poner un dom repeat para ver los todos 
-
~~~ 
  ....
  eripuit sit.</p>
  <template is="dom-repeat" items="[[todos]]">
    <ul>
      <li>{{item.text}}</span>
    </ul>
  </template>


    <div>
~~~ 

inlcuir una nueva propiedad todos en el js 
-
~~~ 
       properties: {
                users: {
                    type: Array
                }
            },
~~~ 

incluir una funcion para que cuando este cargado el componente este cargado lance la peticion ajax 
-
~~~ 

            ready: function () {
                this.$.requestTodos.generateRequest();
            },

incluimos una funcion para procesar la respuesta
-
~~~ 
            handleResponse: function (data) {
                this.set("users", data.detail.response);
            }
~~~ 


para que coja los cambios hay que 
 - ir al directorio client
 - hacer polymer build 
 - volver al directorio padre 
 - npm start 
 - borrar la cache del navegador 
 - abrir en navegador en modo incognito para que no cachee la peticiones 


incluimos lo snuevos archivos que pudiera haber
-
~~~ 
       git add .
~~~
hace run comit para decirle a git que quieres los cambios
-
~~~ 
    git commit -m "modified component my-View1 to call ajax"
~~~
 
subir los cambios al repositorio 
-
~~~ 
    git push -u origin master
~~~
