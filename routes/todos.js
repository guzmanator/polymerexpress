// una lista de items para devolver 
var listItems =
[
    {
        "id": 1,
        "text": "Get Up"
    },
    {
        "id": 2,
        "text": "Brush teeth"
    },
    {
        "id": 3,
        "text": "Get Milk"
    },
    {
        "id": 4,
        "text": "Prepare coffee"
    },
    {
        "id": 5,
        "text": "Have a hot cup of coffee"
    }
];
// un metodo para devolver toda la lista 
exports.list = function(req, res){   
    res.json(listItems);
};

// un metodo para obtener un objketo de la lista por id
exports.get = function(req, res){ 
   var item = {};
   if (req.params.id){
     item = listItems.find(function(item) {
          return item.id == req.params.id;
      });
   }
   if(!item){
      item = {"item":"not found"} 
   }
   res.json(item);
};

// un metodo para incluri un elemento a la lista 
exports.addToList = function (req, res) {
    var maxId = listItems.length;
    var newTodoItem = { "id": maxId.id + 1, "text": req.body.text };
    listItems.push(newTodoItem);
    res.json({ success: true, item: newTodoItem });
};